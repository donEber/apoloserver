const { ApolloServer } = require('apollo-server')
const { makeExecutableSchema } = require('graphql-tools')
const videos = require('./videos')
const typeDefs = `
    type Video{
        id: ID!
        title: String!
        views: Int
    }
    input VideoInput{
        title: String!
        views: Int
    }
    type Query{
        getVideos: [Video]
        getVideo(id: ID!): Video
    }
    type Mutation{
        addVideo( input: VideoInput ):Video
        updateVideo(id: ID!, input: VideoInput ): Video
        deleteVideo(id: ID!): String
    }
`
const resolvers = {
    Query: {
        getVideos(){
            return videos
        },
        getVideo(obj,{ id }) {
            console.log(id)
            return videos.find(v => v.id == id)
        },
    },
    Mutation: {
        addVideo(obj,{ input }) {
            const id = String(videos.length + 1)
            const newVideo = { id, ...input }
            videos.push(newVideo)
            return newVideo
        },
        updateVideo(obj,{ id, input }) {
            const index = videos.findIndex(e => e.id === id)
            const video = videos[index]
            const editedVideo = Object.assign(video, input)
            videos[index] = editedVideo
            return editedVideo
        },
        deleteVideo(obj,{ id }) {
            const index = videos.findIndex(e => e.id === id)
            videos.splice(1, index)
            return `Se elimino el video con id ${id}`
        }
    }
}
const schema = makeExecutableSchema({
    typeDefs,
    resolvers
})
const server = new ApolloServer({
    schema
})

server.listen().then(({ url }) => {
    console.log(`Servidor inicializadoen ${url}`)
})